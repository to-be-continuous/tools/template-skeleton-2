# to-be-continuous template skeleton project

This project allows creating a new to-be-continuous template repository from scratch in seconds using [cookiecutter](https://cookiecutter.readthedocs.io/).

## Usage

1. Install `cookiecutter` globally (requires Python installed):

   ```bash
   pip install cookiecutter
   ```

   :information_source: for other installation options, please have a look at the [official doc](https://cookiecutter.readthedocs.io/en/stable/installation.html).

2. Run cookiecutter to create a new to-be-continuous template project:

   ```bash
   cookiecutter https://gitlab.com/to-be-continuous/tools/template-skeleton-2
   ```

   then answer the CLI questions

3. init the Git repo and share

   ```bash
   cd name-of-template
   git init .
   git remote add origin https://gitlab.com/to-be-continuous/name-of-template.git
   git add .
   git commit -m 'initial commit'
   git push
   ```
